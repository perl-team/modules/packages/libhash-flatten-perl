libhash-flatten-perl (1.19-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libhash-flatten-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 07 Dec 2022 22:42:34 +0000

libhash-flatten-perl (1.19-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 19 Nov 2022 14:29:58 +0000

libhash-flatten-perl (1.19-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 14 Jun 2022 22:16:18 +0100

libhash-flatten-perl (1.19-2) unstable; urgency=medium

  * Team upload.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Niko Tyni ]
  * Update to debhelper compat level 10
  * Update to Standards-Version 4.1.3
  * Declare that the package does not need (fake)root to build
  * Add Testsuite declaration for autopkgtest-pkg-perl

 -- Niko Tyni <ntyni@debian.org>  Sun, 28 Jan 2018 14:56:55 +0200

libhash-flatten-perl (1.19-1) unstable; urgency=low

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ Ansgar Burchardt ]
  * New upstream release.
  * Use tiny debian/rules.
  * Use source format 3.0 (quilt).
  * debian/copyright: Formatting changes; refer to "Debian systems" instead of
    "Debian GNU/Linux systems"; refer to /usr/share/common-licenses/GPL-1.
  * Bump Standards-Version to 3.9.1.
  * Add myself to Uploaders.

 -- Ansgar Burchardt <ansgar@debian.org>  Mon, 11 Oct 2010 16:23:06 +0200

libhash-flatten-perl (1.16-2) unstable; urgency=low

  * debian/copyright: fixed lincence info. Package is licenced GPL-2+ (included
   COPYRIGHT file is GPL version 2 and link mentioned in README points to GPL-3)

 -- Krzysztof Krzyżaniak (eloy) <eloy@debian.org>  Wed, 08 Apr 2009 16:53:22 +0200

libhash-flatten-perl (1.16-1) unstable; urgency=low

  * Initial Release (closes: #523103).

 -- Krzysztof Krzyżaniak (eloy) <eloy@debian.org>  Wed, 8 Apr 2009 15:51:42 +0200
